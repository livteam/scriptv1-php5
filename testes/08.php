<?php
    echo "Digite sua mascara em bits CIDR: ";
    $mascara = fgets(STDIN);

    if ($mascara  == 8) {
        echo "Sua Máscara é de classe 'A' 255.0.0.0" . PHP_EOL;
    }
    elseif ($mascara  == 16) {
        echo "Sua Máscara é de classe 'B' 255.255.0.0" . PHP_EOL;
    }
    elseif ($mascara  == 24) {
        echo "Sua Máscara é de classe 'C' 255.255.255.0" . PHP_EOL;
    }
    else {
        echo "Sua Máscara é sub-rede" . PHP_EOL;
    }
?>

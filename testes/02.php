<?php
    $Algoritmo = "md5";                                 // Variável que define qual algoritmo de hash será usado

    echo "Digite sua Senha: ";
    $Senha_Digitada = fgets(STDIN);
    echo "Você Digitou: ", $Senha_Digitada;
    $Senha_Digitada = trim($Senha_Digitada);            // Remove espaços no inicio e fim
    $Senha_Cifrada = hash($Algoritmo, $Senha_Digitada);
    echo "Senha Cifrada: ", $Senha_Cifrada . PHP_EOL;

    echo "Tamanho do Hash: " . strlen($Senha_Cifrada)*8 . "bits";
?>
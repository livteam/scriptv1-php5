<?php
    echo "Digite um endereço IPv4 no formato decimal: ";
    $ip = fgets(STDIN);

    $byteIP = explode(".", $ip);

    echo $byteIP[0] . PHP_EOL;
    echo $byteIP[1] . PHP_EOL;
    echo $byteIP[2] . PHP_EOL;
    echo $byteIP[3] . PHP_EOL;

    if ($byteIP[0] < 0 || $byteIP[0] > 255) {
        echo "IP Inválido! Byte 01 incorreto." . PHP_EOL;
    }
    elseif ($byteIP[1] < 0 || $byteIP[1] > 255) {
        echo "IP Inválido! Byte 02 incorreto." . PHP_EOL;
    }
    elseif ($byteIP[2] < 0 || $byteIP[2] > 255) {
        echo "IP Inválido! Byte 03 incorreto." . PHP_EOL;
    }
    elseif ($byteIP[3] < 0 || $byteIP[3] > 255) {
        echo "IP Inválido! Byte 04 incorreto." . PHP_EOL;
    }
    else {
        echo "IP Válido!" . PHP_EOL;
    }
?>
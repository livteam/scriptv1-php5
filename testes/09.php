<?php
    echo "Digite a quantidade de bits 1 da máscara (CIDR): ";
    $numBits1 = fgets(STDIN);

    if (1 <= $numBits1 && $numBits1 <= 32) {
        $mask = 0xffffffff;
        $numBits0 = 32 - numBits1;
        $mask = $mask <<  $numBits0;

        echo long2ip($mask);
    }
    elseif ($numBits1  == 0) {
        echo "0.0.0.0" . PHP_EOL;
    }
    else {
        echo "Nº de Bits 1 deve ser entre 0 e 32." . PHP_EOL;
    }
?>
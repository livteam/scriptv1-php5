![Apache PHP](https://lh3.googleusercontent.com/NqhRJ9Q6Av-Y_bbJ6wj1LDnS1vRBEwBT-UO124fkn6hme8n0l2Srdy4Kk-7NlZDWiYYsmeb3sLtn7855nqH6tCoIlhWqWI0W5-azYpWieldCTg1cdpLxfSwoFnLNtm3OW3nX6_xNjGR69B9uNeIeIull5cNttgkffWqULXZHpMU2oxW10FDLgnTId5GQ1V7x-nwac6qJ6nfYtqzO0js--8Gnd58zo_ajcN-N4Owrq7WwTI3NIwcZoXNxtyoYLKbM2bPRFuDoZOwe5ubVx3S8gcfeSMqQ8rP44xuGS_9U0FUI0rCFi3myWsEbBEUiMnetLJt3th2cDnS_4vdaO3s9tkl6SH36_xpzJIror2XMW_EZjL__tJcZz2IES6AFFVv8r8AtDHF4hJTPeVCWRvXtKbvLEA9U9k9XK52yfgem_FtTNkMLyrkWllqVgh3GqPTbcvEUTSSxH0ulBfvaAvOrR3NB4b9msYQoRWlzleVz9YPPXvkYkC3B-cFXZeEu25CshhwNQN-HkRzTZPamj9o9DsUR_YVBrVMSrnjXOkJkhYw45XZ-5FNSFfOECZ1UTucXqbp80IuFQczrybg_ArOtdmYjjrDvaWVD7qoRdIwsL8Jzl-Yu9g=w340-h148-no)




PHP5 - Script v1
===================

Conteúdo sobre exemplos praticos do **PHP5** aplicado na **Faculdade SENAI FATESG** para o treinamento e Qualificação Profissional, feito em 22 de maio de 2015.

----------



[TOC]

## Fundamentos da Programação

> - **Lógica:** Solução de Problemas
> - **Algoritmos:** Problemas Computacionais
> - **Linguagens de Programação:** Tradução de Programas/Execução

![Fundamentos-da-Programacao](https://lh3.googleusercontent.com/DYgy5rGoOJ7kb6IV5mO4Lvt-eZcyHKwpFX032jCa0CokPuckUoCi16_1ur_PCoagBf-azMk7IlVvnvxAa9PuthII60Wxf8ewctoCC6NJvc_q6tJgqDNlthXKXpGKuMhAC3DXoA8Ke0XkbheziwcJxZvlbLsK1WoFouurWGC3KMitHbRQ2rb49HYNds3NBYP6rr3OKafb0o2TiUGHn_j6p0iOEFN9JM_vH9kon_n-DQy0CdniBxpNLAI9DbUikR4XWMHHsOjKTufPYS6Dt9cL4YtA0kvE7jPAvE-lgvM3sSRdR0WxyCurZqlNdONm9vmjoDJdTSuNZjImD90mgpkKyva0HAv4STUwRPDNjV0ZB-diRIXE9VwVrresYzfhiqYxI5I00pGudW7wFsIsFQKfatJq3tYWazKaEF8ma1oCJPByo19EITpNqsNGTHjBxxhZbwKCVHc9UrDsBqV35ghJkJxvnlV8qdHVdmffdw-O4CXm9hBVBzbdEpLiXmYeSGH121hfuBRS5O_9X_ha_Xv2SQdyeP13k2CRI-9zZRJu_5Toi_-g2cAeLG-TKM7BVMBlhpAL2fPX6fkR0c8mq56ZeTN9ZkNTv_zapbZUHYqbwSyXzw=w581-h112-no)

> **Exemplos:**

> - Linguagem de Máquina *Binário*
> - Lingugaem de Montagem *Assembly*

#### Arquitetura

![Exemplo-de-Arquitetura](https://lh3.googleusercontent.com/eyuJAKps-7txplK2mEfFG0p-hq7DmkBDvISxUsWGXx65Ng26g0VXhnzalpd9ElhHBFf3vy-0s5svRu4i-NWgDhIxMOchCUKFAlg8aXMv3ue6cUEJBT6olyOF4nI3hs6asoHl6yx0br_2ACnhzukYVWQ4ye6sKpG5yaHqmpqEmg0whnIvfKgrMV7kudWgga2IK6Jz_wWmmnj4BhpUFLAxl2y_qiBatmyYsR7AvkQH4__cmZpehHnCIxydMghm7MytdeLDLzT-ZcAEUTOztvvBGUMslglMD8ZPezX71MgmkvLjKJyb33FAPbR9p0Hj-e13IbZ3eONoRyvgQapNTzHRDjTmi9JJobhbppawWlVWv5DsrNbMT9IkXj4ceMsxGWxBcNP2vtjEUaIO-8-S-bl8rtxEBJthMiQguu9c-4uotXaaO2n0qwnCELVavh9U1QHo8YmkCVrekouLDHZa8eFMzalMzp8qEOKx_zG_cdJ92yg3kWdJmbsuvM8DVm2JS0Bgw1CynUCykZJdFD-OiLXWVB3oaYbn3k2-dMglxtBOOSzWMi30mwzSC2D9Bn6OlHAbnffA8trn6a7ih_G00vc8nmldRFTpurQ4iNJtGJ3yAwM8gw=w720-h540-no)

> - **Plataforma:**
>
>   Windows/Linux
>
> - **Editores:**
>
>   Notepad++
>
>   Visual Studio Code
>
> - **Instalação:**
>
>   Ao iniciar, vai em:
>   Menu Iniciar > Meu Computador > Botão Direito  >  Propriedades > Configurações avançadas do sistema
>   "Configurações avançadas do sistema":
>
>   Propriedades do sistema > Variáveis de Ambiente > Variáveis de usuário para aluno > Novo…
>
>   Nome da variável: PHP
>   Valor da variável: c:/php/php.exe

----------


Comandos Básicos
-------------------

![Uso-do-PHP](https://lh3.googleusercontent.com/1DkwpVBwDdFZY14GNmmAfKAVWj1SymdQF-vhdTVyWe2_R8rVkSjjdjn-TPKDRXYDS2mmysJtgsr4cWrOiOWKpuOOQswoyrUql5mSrIQnkGlMGmWgfWph0fCRcIdHDMvUURRqJ44KN7nwrQ1lXCiQUa4hsIbCwtVKOZocYUvfYrpAjbbhF58G569tV6E79Y_qW-rYcXqRoxsPn8jkdOs8htQaVGN09Q-KbBLtqDwAxIM3nqdZmfS87dWVftfJ_gVyu7nmf3VJbvW-YcMO4WsdqZcJiyp_MQQ47_6UFWmljG36bsIvD7tPQK1lf6iJrwvatSZYN60M2CZJrMqG_lD9mvkF1qE9VO4XVm3_3tO0z1lg0QBew5NGuxPWy-wShK3FxBXomJh_hygFbaiBwnRELxWZXqFYh4_rcZiK3Me1M8qNGFHVK2PmFITSN0gAPPOXISp03qJrkqCm_pE-HJXPPEJkJJZOTbXRf1fqUuOlDEe0FzrhBI4ygjWUonqeHYg2XtPz_mbDe5jhoSHijv8sn4jcesMvmHjIppY5b1mJVxkdzsO6TVb3F2BI-W_S0TyNThZaPb7_e2Sa538yxb5BH2SMyTXN6lE-Rbt8v2Q5XcuyHQ=w689-h203-no)

#### echo

Escreve texto na Saída (Console)

```php
PHP_EOL # -> Constante para Quebra de Linha;
/n # -> Constante para Quebra de Linha;
. # -> (Operador) Usado para concatenar Texto/Variáveis.
```

#### variável

Local de Armazenamento de Dados

```php
$
```

> **Obs:**
>
> - Armazenar um valor de cada vez (Escalar);
> - Não precisam ser Declaradas.

#### Tipos de dados Escalaveis

- **Integer:** Nº’s Inteiros
- **Float / Double:** Nº’s Reais (Casas Decimais)
- **String:** Texto
- **Boolean:** True / False

#### Tipos Compostos

- **Arrays:**

  ```php
  $x[0],$x[1]
  ```

- **Objetos**

#### Operadores

- **Aritméticos:**

  ```php
  + 	- 	* 	/ 	%
  ```

- **Atribuição:** 

  ```php
  =
  ```

- **Relacionais:** 

  ```php
  < 		> 		== 		=== 	<= 		=> 		!=
  ```

- **Lógicas:** 

  ```php
  & (AND) 	| (OR) 		! (NOT)
  ```

- **Funções (cálculos matemáticos):** 

  ```php
  sin 	cos 	log 	log10
  ```

## Estrutura de condições

Permitem desviar o fluxo de execução do **programa / script** de acordo com o teste de uma condição *pré-determinada*.

![Teste-Lógico](https://lh3.googleusercontent.com/B0BohiPJVIk3z1H9N5Am3FyI66f_1tf4vZyKCY7TuC6Nzkj6RLkDXFZHDx_VyXKf7vICu35iILeqB97V25X2BB9VPuYtGzmeziIazocw-eUkJorCZoZJ4m0jJXnM_99SVzRxZNSCV6PTCckqO1_LFn_rE1YHbYoeCqM7G5Donbl9hU9bgmoVjrPNabM7FzirChZf9phEVuZzouvjSP5ZSc3k-lUjJ_z4huS51E7yuN8t3mnc_Ul4JAUC-A7Mo8viSJ6D0Lg77BYM8gyIXRMNcV_-W2r8bwmOV26B7R7FTh0N43myeUQDuX_082iopnc9l-eD2i5JE8ZUYa6MhozMAxgxYFJFlRMgmGKmXh_koLmTfybYtmiPiWn9G4NdmpQR_eHkjzEJb1NN0Y4zfXdHWdElCljeVQKyr_9UO1akKUYaUlcK7WYNf6vcTfL2ABjFFXeqRiT-Dqu2Ymn2tyKvSUODa08TpP4BHQvB-SC4RicZfyFnHImiDyIdjcOLCM-UXKCyLkh20hrBLOy30M7ZkhVMFmJtFmtv6-Xy-0MYnma4PHjo3HmG5qqflnzqgJ0ItIsfKfC5YAzu_mKK9Ggq0Le8IkgmoHDBHs3ORGqj51UrIA=w720-h540-no)

#### IF

![IF](https://lh3.googleusercontent.com/p-ZUZCI2UF7a4k5GvOjtINMzxidxTDTWkJN77MQEk5bprtmn6WtS8DSh5NO_TIMmPBvIMUCvMpBMIM1NBvby5UX-cU6L7b8cFgQW9sJTnQpmyGqROy_L_HfivINQlfyU7nopjeHtPdJPv2yHwy5o1nMUGMY5auZeWqwYNod2xj-XgCL165DksDhQvNLE623Js7qminVEaNzsDtFoIGMd4OHXL-nhPRx4XwTSmLIiRyFnxdGR4SbUScsmpwo7SPQGjQ_6qBzI-QhlrsDKC6-pMb0xRv-np7N4Y_RuGV7MOMeqJ79lT-1Y_BKBpZ5zLB46MIGbdI1_ITH-JVmcIlZW2fCm9xg_tDGQ0vb8xUNTZy9zmOaHp6g3fBqoB33VS3-JNJ8DPfRScSM-XN0QoIY6q2NQhzASY3rrPz8TYNB8atkTFkOk7odDj-E0_9ZewS8yoXMDZocPh0v9-n_MgQggfQVyX40umlFcx8GZMzzdnZS1p0kTEnVZ7Az66_E8t7ylGwXkEG82lPxIpJ3QZct45FTyFb1YsZtcViL5tS88jSpuy2heVMx0QDnBaldU0gEHIBuF5UanwMRTcrNR9gloUoiJ1Pcfm5ysv9AZVxj_kcc5ow=w608-h107-no)

#### IF - ELSE

![IF-ELSE](https://lh3.googleusercontent.com/TeE_mwGuS2IE0vK5J9KpixgmFIzyvE1mc4yoFM-Vi68fYHbJxsrHqiT--N7DpYtHJCNUgQ5ZhjYDfOLyDW9mrL5ErTw58mLDDT1wJqAzEq3dwtRc1rAlwCUK2wW2aHs-3VuJfcrGkAC1t1lWlcNNcKhyygrR4S_k_VrvlkbO1kO1y9Zsso35RfktMlnnSaCOj7MZC8x-ZPwp1lqD1oXbvesOYoXHDLA_R1F2FTswHNMMgltTsmBKst6Rpd2RPuKXb8-bPYAfwdv3t7LH94gAZLm6_wZDPg8c8dtHSPg5NOe_qL9tPNwiz1GTrisSomgJAynbvnEjWjNsKB0cgokMYiuZ4qX3wZIkwmiJF1d37spsZVHsjNgo0QdAjRRA4AO_bkZbPRNiPxRPsl35nYdb-iFQmpkZuhGvilY6QfWi1B7O9Wjrsfm90dRvZDljG0L34LMgokAv1gW5wDvnDVXrd8HeoONmezZxXImTJCSecfv21RtHinEzGHnBU774vtc6YtdvhOZN_E5CxFnW1iTsNcQNI-YkdOdhoFtvh6W_Gw1Un_D9gBruJUJS6S6JHFkz_IHuTAMwxZ-8kV8OTVU2vS4s1OGn61EeYTZbkRY3Q28MsQ=w344-h174-no)

#### IF - ELSEIF - ELSE

![IF-ELSEIF-ELSE](https://lh3.googleusercontent.com/DSia9I4nqFNk_fZ2hn38-TU7mV-tf2hSkna_s71Ax99RrK_poS7t7ai1zRsXZ_UmmTu_XQMHrXsSdZLFI9ZdcDKlwdwnqNWha7yUSELpxV3zFAgEEHtZui3QRNQzA8S5n-Z7YYrq_xx7rqrUG2F5K8Ywy4k4CaYB2qNy0oAjXJWXcbRrWUk8nQ5eXjwZGKxH0WMo3ZEqhl1Hw_yJdeBPslVqjbYsneAf1vdReQfplzEc1Z9Xsd_yeXu08NaVWYtvE2cZaSv277SuLj0ne0EzjgfsYr2E4kcWXNMfLcSvmpAvhOBF7Z_0tjVOfqaKMfs38dFwL2014ufS6XD2feA7bBxfYEo7OLSR4WqCAFSeP_Jg0hdBdMz0XyyjSDk1PN86nwBp-7ReRODFCZhPM2hp5lKrc6d-uVzomca9OcX3LZ0yCnh6-K2VP93lD3x2YR9M3MAUo25wj0vD2CfNZbiM9HQLNv-ChoXsAOZCemTinrRxcdVSXR1ctNbQcNA5M4nwDvUVoavfTB5Wj-BIoEGv-tRLALNCOTdpbvwwlN5J_AU8jADsxBcW-vceBbvKOZRojBo4QunDf4GUKBKTHWREQ3nB-GUSkpMcYKAKVAI8Gnrf3g=w535-h375-no)

#### SWITCH

![SWITCH](https://lh3.googleusercontent.com/yAQeh2R_46tfHCC3nqylwYWt-kUmvVtB1Ge71M_29cl7jO6C8RkfwHuF91JoM8tZ0ehZuJVn8lgzPeDBe5JJ52gE_AVV5s5wPaPFEoBe97QFz1lUI3re00__NKlc3yyWcI6bWYTrkoOMevnf7OTX7Zioeo3a1fLu0QGc5Tt6nvcqfu-mWLLNh_Tl-1Vr8PZtZiJXZXjusRobR7LmJmn06KUVZ6wrZ2usbaPAUJI4XFSlXS63mVl2rF-lXqnZ-wkQA1-YFpb39nClFl8ewkPMrONKDuFw3-OgUztdL9NORm6oW-nW5F-MzLkC6c6VGqvvk3-hLK74vLdgu62PRkl7YJj-rJ_Kt-T41AuyZZTmbrFuOEcNQU08QITIGx_ffHKroFEZnQkgDVZhz8TfC_R_4Epal3lqTqgwFfZwOFvrp8DepMq4C_9ivCK5YjA4mf9HkakksjWYmCbIawhbIAVRat8ZqQ_XJu8AKW5AHS6DZ-IqHqv7JSRAzuIZFo0DjQV2Gj34CCCKDIszpkHtsY0E4j2aC1HSAcmqrA0I_bwX4P445l4TLyKPbNlmC7s2vd07uq8sXPXtAkZI1Z79rdpRVPREWawj4hzUoWUcCbi3mV7V3A=w157-h376-no)

Direitos autorais e Licença
-------------

Este trabalho não foi modificado de seus Criadores (Link's de consulta abaixo), foi adaptado de acordo com a documentação do mesmo e dando os créditos contida neste repositório, a busca e a organização para futuras atualizações deve ser dado ao **contributors.txt** (BY).

Este trabalho foi escrito por Leonardo Cavalcante Carvalho e está licenciado com uma [Licença **MIT**](https://opensource.org/licenses/MIT).

[^stackedit]: [StackEdit](https://stackedit.io/) is a full-featured, open-source Markdown editor based on PageDown, the Markdown library used by Stack Overflow and the other Stack Exchange sites.
[^PHP5]: Manual de instalação oficial do [php.net](http://php.net/manual/en/install.unix.apache2.php).
